# Scoop Bucket

This is the official Scoop Bucket for the TBlock ad-blocker

## How to enable

```
> scoop bucket add tblock https://codeberg.org/tblock/bucket
```

## Installing TBlock

```
> scoop install tblock/tblock
```

